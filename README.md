# HSSE DB-2024 HW

Репозиторий с домашними заданиями по курсу "Базы данных" ВШПИ.

Общая информация по дедлайнам, кодстайлу и тому, как заливать задачи, расположена
в [репозитории курса на gitlab](https://gitlab.com/destitutiones/hsse-db-course/-/blob/main/sql_homeworks/README.md?ref_type=heads).